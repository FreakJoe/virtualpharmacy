angular
.module('VirtualPharmacy', ['ngMaterial'])
.config(function($mdThemingProvider) {

	$mdThemingProvider.theme('default')
	.primaryPalette('indigo')
	.accentPalette('light-blue')
	.warnPalette('red')
	.backgroundPalette('grey');

})
.service('medicationService', function(){

	var medication = 0;
	var setMedication = function(med) {

		medication = med;

	};
	var getMedication = function() {

		return medication;

	};

	return {

		setMedication: setMedication,
		getMedication: getMedication

	};

})
.controller('AppController', function($scope, $timeout, $mdSidenav, $log) {

	$scope.toggleSidenav = buildToggler('left');
	$scope.isOpenSidenav = function() {

		return $mdSidenav('left').isOpen();

	};
	function buildToggler(navID) {

		return function() {

			$mdSidenav(navID).toggle();

		};

	};
})
.controller('MedicationController', function($scope, $http, $mdDialog, $mdMedia, medicationService) {

	$http.get("api/medications/getMedication/ALL").success(function(response) {

		$scope.medications = response.medications;

	});
	$scope.showEdit = function(medication) {

		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
		$mdDialog.show({

			controller: DialogController,
			templateUrl: 'editMedication',
			parent: angular.element(document.body),
			clickOutsideToClose:true,
			fullscreen: useFullScreen

		});
		medicationService.setMedication(medication);

	};
	$scope.showView = function(medication) {

		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
		$mdDialog.show({

			controller: DialogController,
			templateUrl: 'viewMedication',
			parent: angular.element(document.body),
			clickOutsideToClose:true,
			fullscreen: useFullScreen

		});
		medicationService.setMedication(medication);

	};
	$scope.addEntry = function(medication) {

		console.log($scope.medication.name);
		console.log($scope.medication.indications);
		console.log($scope.medication.sideEffects);

	};		
	

})
.controller('MedicationEditController', function($scope, $http, $mdDialog, $mdMedia, medicationService, $mdToast) {

	$scope.medication = medicationService.getMedication();
	$scope.editMedication = function() {

		var url= 'api/medications/editMedication/';
		url = url + $scope.medication.id + "/";
		url = url + $scope.medication.name + "/";
		url = url + $scope.medication.indications + "/";
		url = url + $scope.medication.sideEffects;
		console.log(url);
		$http.put(url).success(function(response) {

			$mdToast.show(
				$mdToast.simple()
				.textContent('Successfully edited!')
				.position("top right")
				.hideDelay(3000)
			);
			
		})
		.error(function(response) {

			$mdToast.show(
				$mdToast.simple()
				.textContent('Something went wrong. If this issue persists, let us know.')
				.position("top right")
				.hideDelay(3000)
			);
			
		});

	};
});

function DialogController($scope, $mdDialog) {

	$scope.hide = function() {

		$mdDialog.hide();

	};
	$scope.cancel = function() {

		$mdDialog.cancel();

	};
	$scope.answer = function(answer) {

		$mdDialog.hide(answer);

	};

}