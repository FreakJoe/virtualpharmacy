import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, jsonify
from flask.ext.restful import Resource, Api
from flask.ext.sqlalchemy import SQLAlchemy

class GetMedicationHandler(Resource):
	def get(self, idP):
		from app import db
		import models
		if idP != 'ALL':
			print("Hello")
			medication = db.session.query(models.Medication).get(idP)
			print(medication)
			medication = [
				{
					'id': medication.id, 
					'name': medication.name, 
					'indications': medication.indications, 
					'sideEffects': medication.sideEffects
				}
			]
			medication = {'medications': medication}
			return jsonify(medication)

		medications = db.session.query(models.Medication)
		medications = [
			{
				'id': medication.id, 
				'name': medication.name, 
				'indications': medication.indications, 
				'sideEffects': medication.sideEffects
			} for medication in medications
		]
		medications = {'medications': medications}
		return jsonify(medications)

class EditMedicationHandler(Resource):
	def put(self, id, name, indications, sideEffects):
		from app import db
		import models
		medication = db.session.query(models.Medication).get(id)
		medication.name = name
		medication.indications = indications
		medication.sideEffects = sideEffects
		db.session.commit()

		return None