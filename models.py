from app import db

class Medication(db.Model):
	__tablename__ = 'medications'

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String, nullable=False)
	indications = db.Column(db.String, nullable=False)
	sideEffects = db.Column(db.String, nullable=False)
	createdBy = db.Column(db.String, nullable=True)

	def __init__(self, name, indications, sideEffects):
		self.name = name
		self.indications = indications
		self.sideEffects = sideEffects

	def __repr__(self):
		return self.name

class User(db.Model):
	__tablename__ = 'users'

	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(25), nullable=False)
	password = db.Column(db.String(255), nullable=False)

	def __init__(self, username, password):
		self.username = username
		self.password = password

	def __repr__(self):
		return self.username