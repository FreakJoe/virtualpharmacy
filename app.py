import sqlite3, os

from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, jsonify
from flask.ext.restful import Api
from flask.ext.sqlalchemy import SQLAlchemy

from apiHandlers import GetMedicationHandler, EditMedicationHandler
import db_create

dbType = 'postgresql'

DEBUG = False
SECRET_KEY = os.environ['VIRTUALPHARMACY_SECRET_KEY']
USERNAME = os.environ['VIRTUALPHARMACY_USERNAME']
PASSWORD = os.environ['VIRTUALPHARMACY_PASSWORD']

SQLALCHEMY_TRACK_MODIFICATIONS = True

if dbType == 'sqlite':
    baseDir = os.path.abspath(os.path.dirname(__file__))
    DATABASE = 'virtualpharmacy.db'
    DATABASE_PATH = os.path.join(baseDir, DATABASE)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE_PATH

elif dbType == 'postgresql':
    SQLALCHEMY_DATABASE_URI = os.environ['VIRTUALPHARMACY_HEROKU_URI']

app = Flask(__name__)
app.config.from_object(__name__)
api = Api(app)
db = SQLAlchemy(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
	return render_template('about.html')

@app.route('/contactus')
def contactus():
	return render_template('about.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    import models

    invalidUsername = False
    invalidPassword = False

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        print(username, password)
        user = db.session.query(models.User).filter(
            models.User.username == username
        ).first()
        print(user)

        if not user:
            invalidUsername = True

        elif password is not user.password:
            invalidPassword = True

        else:
            session['logged_in'] = True
            return redirect(url_for('index'))


    return render_template('login.html', invalidUsername=invalidUsername, invalidPassword=invalidPassword)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('index'))

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/editMedication')
def editMedication():
	return render_template('editMedication.html')

@app.route('/viewMedication')
def viewMedication():
    return render_template('viewMedication.html')

api.add_resource(GetMedicationHandler, '/api/medications/getMedication/<string:idP>')
api.add_resource(EditMedicationHandler, '/api/medications/editMedication/<string:id>/<string:name>/<string:indications>/<string:sideEffects>/')

if __name__ == '__main__':
    db_create.create()
    app.run()